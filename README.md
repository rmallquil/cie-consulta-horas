# CIE - WEB HORAS
	
Consta de dos aplicaciones web para el Centro de Emprendimiento

* cie-ar
* cie-as

Lo que hace es que el estudiante ingrese su código y este le permita visualizar en que actividades participó y cuantas horas acumuló.

* Node Js
* Angular 2.6
* MSQL.

Ahora, la data se extrae del BDCIE y BDUCCI, para ello se realizaron pruebas en el servidor .53 y se ha creado un usuario prueba de aplicación:

* User: ssAppCIEWeb

debe contar con la conexión al servidor de producción (.206), el archivo de configuración se ubica en la siguiente ruta: cie-ar\config  --> config.js

### Preparacion para publicación e inicializacion -- T.I.C.

    * cie-ar/ ```node index.js```
    * cie-as/cief/ ```ng serve --host=0.0.0.0```
	
## Configuracion

Existe un archivo de configuracion previa en `cie-ar/config/config.js` 

## FAQ

*Aplicaciones elaboradas por Arnold Augusto Raymundo Vivar araymundo@continental.edu.pe 
*repositorio de respaldo para la aplicación asignado por T.I.C.

`- git@gitlab.com:rmallqui/cie-consulta-horas.git `





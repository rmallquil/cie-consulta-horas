
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, RequestOptions } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
//import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';

//import { LoginService } from './services/login.service';
import { FormsModule } from '@angular/forms';
import { AdminComponent } from './components/admin/admin.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './guard/auth.guard';
import { AuthService } from './services/auth.service';
import { BusinesshoursService } from './services/businesshours.service';
import { BusinesshoursComponent } from './components/businesshours/businesshours.component';
import { ComponentNameComponent } from './component-name/component-name.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    AdminComponent,
    HomeComponent,
    BusinesshoursComponent,
    ComponentNameComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    //AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path:'', component:DashboardComponent },     
      //{ path:'login' , component:LoginComponent },
      { path:'dashboard', component:DashboardComponent, //canActivate:[AuthGuard],
            children:[{path: 'rpt/business-hours', component: BusinesshoursComponent}]
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
    ])
  ],
  providers: [AuthService,AuthGuard,BusinesshoursService],
  bootstrap: [AppComponent]
})

export class AppModule { }

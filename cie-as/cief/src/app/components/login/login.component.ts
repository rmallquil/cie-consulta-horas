
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login:any = [];
  idUser:string;
  userPwd:string;  
  body:any = {
    idUser:undefined,
    userPwd:undefined        
  };  
  constructor(
     public loginService:LoginService,
     public router:Router,
     public auth:AuthService
  ) {
      
   }

  ngOnInit() {
  }

  loginUser(event){
    event.preventDefault();
    this.auth.getUserDetalis(this.idUser,this.userPwd).subscribe(data => {
      if(data.data.ingreso == '1'){
        //redirecciona a la persona a ADMIN
        //alert('Ingresando a ADMIN!!!!');
        localStorage.setItem("username",this.idUser);
        this.router.navigate(['dashboard']);
        this.auth.setLoggedIn(true);
      }else{
        window.alert('Credenciales incorrectas!!! ');
        this.auth.setLoggedIn(false);
        this.idUser = '';
        this.userPwd = '';
        this.router.navigate(['login']);
      }
    });    
  }  
}

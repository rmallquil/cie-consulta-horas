import { TestBed, inject } from '@angular/core/testing';

import { BusinesshoursService } from './businesshours.service';

describe('BusinesshoursService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusinesshoursService]
    });
  });

  it('should be created', inject([BusinesshoursService], (service: BusinesshoursService) => {
    expect(service).toBeTruthy();
  }));
});
